package com.example.practica01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.os.bundleOf

class OperacionesActivity : AppCompatActivity() {
    private lateinit var txtUsuario : TextView
    private lateinit var txtNum1 : EditText
    private lateinit var txtNum2 : EditText
    private lateinit var txtResultado : TextView

    private lateinit var btnSumar : Button
    private lateinit var btnRestar : Button
    private lateinit var btnMult : Button
    private lateinit var btnDiv : Button

    private lateinit var btnLimpiar : Button
    private lateinit var btnSalir : Button

    private lateinit var operaciones: Operaciones

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operaciones)
    }

    public fun iniciarComponentes(){

        txtUsuario = findViewById(R.id.txtUsuario)
        txtResultado = findViewById(R.id.txtResultado)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)

        btnSumar = findViewById(R.id.btnSuma)
        btnRestar = findViewById(R.id.btnRes)
        btnMult = findViewById(R.id.btnMul)
        btnDiv = findViewById(R.id.btnDiv)

        btnSalir = findViewById(R.id.btnSalir)
        btnLimpiar = findViewById(R.id.btnLimpiar)

        var bundle : Bundle = intent.extrastxtUsuario.text = bundle?.getString("Nombre")



    }
    public fun validar() : Boolean {
        if(txtNum1.text.toString().contentEquals("") || txtNum2.text.toString().contentEquals("")) return false

        else return true
    }
}